package schemas

import "github.com/gin-contrib/cors"

type AppConfig struct {
	Debug        bool
	Host         string
	Port         string
	UseTLS       bool
	CertFilePath string
	KeyFilePath  string
	Cors         *cors.Config
}

type DatabaseConfig struct {
	Host     string
	Port     string
	Username string
	Password string
	Database string
	Dialect  string
	TimeZone string
	SSLMode  string
}

type RedisConfig struct {
	Host     string
	Password string
	DB       int
}

type SMTPConfig struct {
	Host     string
	Port     int
	Email    string
	Password string
	Name     string
}

type OneSignalConfig struct {
	AppId      string
	RestApiKey string
}

type NewRelicConfig struct {
	AppName string
	License string
}

type MinioConfig struct {
	Host          string
	Location      string
	AccessKey     string
	SecretKey     string
	SSL           bool
	ReplaceDomain string
}
type SchemaEnvironment struct {
	DB_USER      string
	DB_PASS      string
	DB_HOST      string
	DB_PORT      string
	DB_NAME      string
	DB_SSLMODE   string
	TIMEZONE     string
	SERVICE_NAME string
	VERSION      string
	REST_PORT    string
	GO_ENV       string
	SWAGGER_HOST string
	JWT_SECRET   string
}
