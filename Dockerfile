

# Create a minimal Docker image with only the binary
FROM ubuntu:20.04

# Copy the binary from the previous build stage
COPY coba-gin coba-gin

# Set the entry point to the binary
ENTRYPOINT ["/coba-gin"]

# Set the default command for the container
CMD ["./coba-gin"]
