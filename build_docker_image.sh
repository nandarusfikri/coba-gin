#!/bin/bash

echo "Build Docker Image Started"
docker build --no-cache -t coba-gin .
docker save -o image-coba-gin.tar coba-gin

echo "Deploy coba-gin Finished"