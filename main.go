package main

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/joho/godotenv"
	"github.com/nandarusfikri/coba-gin/schemas"
	"github.com/sirupsen/logrus"
	"os"
	"strconv"
)

var ConfigEnv schemas.SchemaEnvironment

//func main() {
//	// Membaca sertifikat SSL dan kunci
//	cert, err := tls.LoadX509KeyPair("/home/sysadmin/vnetcloud_2022-2023/vnetcloud_2022-2023.crt", "/home/sysadmin/vnetcloud_2022-2023/vnetcloud_2022-2023.key")
//	if err != nil {
//		fmt.Println("Failed to read SSL certificate and key:", err)
//		return
//	}
//
//	// Mengkonfigurasi server TLS
//	config := &tls.Config{
//		Certificates: []tls.Certificate{cert},
//	}
//
//	router := gin.Default()
//
//	// Menjalankan server dengan TLS
//	server := &http.Server{
//		Addr:      ":443",
//		Handler:   router,
//		TLSConfig: config,
//	}
//	err = server.ListenAndServeTLS("", "")
//	if err != nil {
//		fmt.Println("Failed to start HTTPS server:", err)
//		return
//	}
//}

func main() {

	ConfigEnv = Environment()
	RESTPort, err := strconv.Atoi(ConfigEnv.REST_PORT)
	if err != nil {
		logrus.Errorln("REST_PORT is not valid")
	}

	r := gin.Default()

	r.GET("/", func(c *gin.Context) {
		c.JSON(200, gin.H{
			"version": "1.0.0",
			"message": "Hello, Gin!",
		})
	})

	if RESTPort > 0 {
		r.Run(fmt.Sprintf(":%v", RESTPort))
	} else {
		r.Run(":2228")
	}

}
func Environment() (config schemas.SchemaEnvironment) {
	err := godotenv.Load(".env")
	if err != nil {
		fmt.Println("Error loading .env file")
	}

	// Read environment variables from docker-compose.yml
	config.DB_USER = os.Getenv("DB_USER")
	config.DB_NAME = os.Getenv("DB_NAME")
	config.DB_PASS = os.Getenv("DB_PASS")
	config.DB_HOST = os.Getenv("DB_HOST")
	config.DB_PORT = os.Getenv("DB_PORT")
	config.DB_SSLMODE = os.Getenv("DB_SSLMODE")
	config.TIMEZONE = os.Getenv("TIMEZONE")
	config.SERVICE_NAME = os.Getenv("SERVICE_NAME")
	config.VERSION = os.Getenv("VERSION")
	config.REST_PORT = os.Getenv("REST_PORT")
	config.GO_ENV = os.Getenv("GO_ENV")
	config.SWAGGER_HOST = os.Getenv("SWAGGER_HOST")
	config.JWT_SECRET = os.Getenv("JWT_SECRET")

	fmt.Printf("Environment sini: %+v\n", config)

	return config
}
